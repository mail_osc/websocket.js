/**
 * websocket 工具
 * 作者：管雷鸣
 * 开源仓库及使用说明：https://gitee.com/mail_osc/websocket.js
 * 带有：
 * 		1. 断线重连（正常挂着时，断线自动重连； 发送消息时，断线自动重连，并将之前的消息在连接成功后发送）
 * 		2. 内置心跳（自动发送心跳消息；用onmessage接收消息时会自动过滤掉心跳消息，也就是服务端的心跳消息不会进入重写的 onmessage 方法）
 */
var websocket = {
	version:'v1.0.0.20221128',	//当前版本
	WebSocket : WebSocket,
	ws:null,
	url:null,	//WebSocket的url
	
	constructor:function(url){
		this.url = url;
		console.log(url);
	},
	
	//连接成功时触发
	onopen:function(){
		/*
		this.ws.send(JSON.stringify({
	        'type': 'CONNECT' //第一次联通，登录
	        ,'token':'123'
	    })); 
		*/
	},
	
	//监听收到的消息的function  function(data){}
	onmessage(func){ 
		var message = JSON.parse(res.data);
		if(message.type == null){
			console.log('message.type == null, message is ignore ! body: '+res.data);
			return;
		}
		if(message.type != null && message.type == 'HEARTBEAT'){
			//心跳消息，忽略
			return;
		}
		
		func(message);
		
		//通知提醒
		if(typeof(message) != 'undefined'){
			//message.text 有值，那么才算是正常的通知消息，播放消息通知
			//kefu.notification.execute('您有新消息',message.text);
		}
	},
	
		
	
	connecting:false,	//当前websocket是否是正在连接中,断线重连使用
	//连接、以及断线重连都是用这个
	connect:function(obj){
		if(typeof(obj) != 'undefined'){
			if(typeof(obj.url) == 'string'){
			this.url = obj.url;
			}
			if(typeof(obj.onopen) == 'function'){
				this.onopen = obj.onopen;
			}
			if(typeof(obj.onmessage) == 'function'){
				this.onmessage = obj.onmessage;
			}
		}
		
		if(this.url == null){
			console.log('======  error ======');
			console.log('Please pass in the url parameter for the connect method');
			console.log('  ');
			return;
		}
		
		if(!this.connecting){
			console.log('socket connect ... '+new Date().toLocaleString());
			this.connecting = true;	//标记已经有socket正在尝试连接了
			
			this.ws = new websocket.WebSocket(this.url);
			this.ws.onopen = function(){
				//开启心跳
				websocket.heartBeat.startHeartBeat();
				
				//执行用户自定义的方法
				websocket.onopen();
			};
			this.ws.onmessage = function(res){
				var message = JSON.parse(res.data);
				if(message.type != null && message.type == 'HEARTBEAT'){
					//心跳消息，忽略
					return;
				}
				
				//res为接受到的值，如 {"emit": "messageName", "data": {}}
				websocket.onmessage(message);
			};
			
			this.connecting = false;
			
			//socket断线重连
	        var socketCloseAgainConnectInterval = setInterval(function(){
	        	if(websocket.ws.readyState == websocket.ws.CLOSED){
	                console.log('socketCloseAgainConnectInterval : socket closed , again connect ...');
	                websocket.connect();
	            }
	        }, 2000);
		}else{
			console.log('In the process of connecting... ignore');
		}
	},
	
	//发送消息
	send:function(text){
		if(this.ws.readyState != this.ws.OPEN){
			//通道没有正常连接通，先连接通道
			
			console.log('socket 通道异常，正在开启重连');
			setTimeout(function(){
				//doSomething(这里写时间到之后需要去做的事情)
				websocket.connect();
				websocket.send(text);	//重新发送
			}, 200);
			return;
		}
		
		//通道正常连接状态，正常发送消息。
		if(typeof(text) == 'object'){
			text = JSON.stringify(text);
		}
		this.ws.send(text);
	},
	
	//心跳相关
	heartBeat:{
		time:40, 	//心跳时间，40秒，单位是秒。每隔40秒自动发一次心跳
		text:'{"type":"HEARTBEAT","text":"AreYouThere"}',	//心跳发起，询问服务端的心跳内容，默认是 {"type":"HEARTBEAT","text":"AreYouThere"}
		isStart:false,	//当前自动发送心跳是否启动了， false：未启动，  true：已启动
		startHeartBeat:function(){
			console.log('---');
			if(this.isStart == false){
				//未启动，那么启动心跳
		        var socketHeartBeatInterval = setInterval(function(){
					//TODO - 可能发不出去
		        	websocket.send(websocket.heartBeat.text);
		        }, this.time*1000);
		        this.isStart = true;
		        console.log('websocket.js headrtBeat thread start , time: '+this.time+'s');
			}else{
				console.log('The heartbeat has started. You do not need to start it again');
			}
		}
	}
	
}

