websocket.js WEB端WebSocket工具类。  
本身的初衷是给同事做的，同事没操作过WebSocket这块，避免他掉坑里再往外爬，我把坑提前给他填了一下，让他直接拿来使用，做好后感觉不能就这么浪费了，直接放出去吧，于是 websocket.js 就有了。 它比原生 WebSocket 其实就只是多了断线自动重连、心跳自动发送的内置，别的就跟正常的WebSocket一样。**让你将精力放在业务逻辑实现上，无需消耗时间弄明白它到底是怎么做到的**。

## 1. 功能介绍
#### 1.1 断线重连
* 正常挂着时，断线自动重连
* 发送消息时，断线自动重连，并将之前的消息在连接成功后发送
#### 1.2 心跳内置
* 自动发送心跳消息
* 用 onmessage 接收消息时会自动过滤掉心跳消息，也就是服务端的心跳消息不会进入到重写的 onmessage 方法

## 2. 代码使用

#### 2.1 引入js

````
<script src="https://res.zvo.cn/websocket.js/websocket.js"></script>
````

#### 2.2 代码

````
websocket.connect({
	url:'wss://api.kefu.leimingyun.com/websocket', //服务端的WebSocket地址
	onopen:function(){ //如果不需要，此不传即可。
		websocket.send({"type":"CONNECT","token":"123456"}); //当WebSocket通道连接成功后，自动给服务器发送一条json格式的消息
	},
	onmessage:function(message){ //如果不需要，此不传即可
		console.log(message); //服务端发送来的消息会在这里接收到
	}
});
````

## 3.流程图示

![](else/flowchart.png)

## 4. 接口说明
#### 4.1 websocket.send(...) 向服务端发消息

其中的参数可以是string格式，也可以是json格式，自动识别。建议json格式  

#### 4.2 websocket.heartBeat.text 心跳消息内容

可自定义向服务端发送心跳消息的内容。默认值为： ````{"type":"HEARTBEAT","text":"AreYouThere"}````   
设置方式如：  

````
websocket.heartBeat.text = '{"type":"HEARTBEAT","text":"AreYouThere"}';
````

#### 4.3 websocket.heartBeat.time 心跳消息间隔时间
可自定义向服务端发送心跳消息的间隔时间，单位是秒，每间隔多少秒自动给服务端发送一条心跳消息。默认值是40，也就是40秒。  
设置方式如：  

````
websocket.heartBeat.time = 40;
````

#### 4.4 websocket.ws JS本身的WebSocket对象
可直接使用 websocket.ws 来获取其JS本身的WebSocket对象来进行更多使用

## 5. 相关项目
[websocket.java 服务端的websocket实现](https://gitee.com/mail_osc/websocket.java)  
[notification.js WEB端（浏览器）通知提醒](https://gitee.com/mail_osc/notification.js)


## 6. 关于我们
作者：管雷鸣  
微信：xnx3com  
QQ群：334981595